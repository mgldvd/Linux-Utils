git status --short --untracked-files=all | awk '
    BEGIN {
        RED="\033[0;31m";
        GREEN="\033[0;32m";
        YELLOW="\033[0;33m";
        WHITE="\033[0;37m";
        RESET="\033[0m";
    }
    /^ M/ {mod[NR]=$2}
    /^ D/ {del[NR]=$2}
    /^\?\?/ {untracked[NR]=$2}
    function print_section(title, color, array, size) {
        if (size) {
            printf "\n%s%s:%s\n", color, title, RESET
            n = asort(array)
            for (i = 1; i <= n; i++) printf " %s⯀ %s%s\n", color, WHITE array[i], RESET
        }
    }
    END {
        if (length(mod) == 0 && length(del) == 0 && length(untracked) == 0) {
            printf "\n%sNothing to commit, working tree clean%s\n", GREEN, RESET
        } else {
            print_section("✏️  Modified", YELLOW, mod, length(mod))
            print_section("🗑️  Deleted", RED, del, length(del))
            print_section("📗 Untracked", GREEN, untracked, length(untracked))
        }
    }'
