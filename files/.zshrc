# | :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
DISABLE_AUTO_UPDATE="false"
DISABLE_UPDATE_PROMPT="true"
# | :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: Antigen  ::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
source $HOME/.antigen.zsh
antigen use oh-my-zsh
antigen theme robbyrussell
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-completions
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-history-substring-search
antigen bundle zpm-zsh/ls
antigen bundle MichaelAquilina/zsh-auto-notify
antigen apply
bindkey '^ ' autosuggest-accept
# | ::::::: Antigen  ::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: alias and path ::::::::::::::::::::::::::::::::::::::::::::::::: >>>
[ -f "$HOME/.custom_alias" ] && source "$HOME/.custom_alias"
[ -f "$HOME/.custom_path" ] && source "$HOME/.custom_path"
[ -f "$HOME/.custom_env" ] && source "$HOME/.custom_env"

if [ -d "$HOME/.alias" ]; then
    for file in "$HOME/.alias/alias"*; do
        [ -f "$file" ] && source "$file"
    done
fi
# | ::::::: alias and path ::::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: title :::::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
emojis=("🍄" "🍇" "🍈" "🍉" "🍊" "🍋" "🍌" "🍍"
        "🥭" "🍎" "🍏" "🍐" "🍑" "🍒" "🍓" "🫐"
        "🥝" "🍅" "🫒" "🥥" "🥑" "🍆" "🥔" "🥕"
        "🌽" "🌶" "🫑" "🥒" "🥬" "🥦" "🧄" "🧅"
        "🥜" "🫘" "🌰" "🫚" "🫛" "🍞")

selected_emoji="${emojis[RANDOM % ${#emojis[@]}]}"

set_win_title(){
    current_dir=$(basename "$PWD")
    echo -ne "\033]0;$selected_emoji  $current_dir  $selected_emoji \007"
}

precmd_functions+=(set_win_title)
# | ::::::: title :::::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: starship ::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
[ -x "$(command -v starship)" ] && eval "$(starship init zsh)"
# | ::::::: starship ::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: nvm :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && source "$NVM_DIR/nvm.sh"
[ -s "$NVM_DIR/bash_completion" ] && source "$NVM_DIR/bash_completion"
# | ::::::: nvm :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: bun :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
[[ -f "$HOME/.bun/_bun" ]] && source "$HOME/.bun/_bun"
# | ::::::: bun :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: DENO completions ::::::::::::::::::::::::::::::::::::::::::::::: >>>
if [[ ! "$FPATH" =~ (^|:)$HOME/.zsh/completions(:|$) ]]; then
    export FPATH="$HOME/.zsh/completions:$FPATH"
fi
# | ::::::: DENO completions ::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: DENO environment :::::::::::::::::::::::::::::::::::::::::::::::: >>>
[[ -f "$HOME/.deno/env" ]] && source "$HOME/.deno/env"
# | ::::::: DENO environment :::::::::::::::::::::::::::::::::::::::::::::::: <<<

# | ::::::: pnpm :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: >>>
export PNPM_HOME="$HOME/.local/share/pnpm"
if [[ ":$PATH:" != *":$PNPM_HOME:"* ]]; then
    export PATH="$PNPM_HOME:$PATH"
fi
# | ::::::: pnpm :::::::::::::::::::::::::::::::::::::::::::::::::::::::::::: <<<