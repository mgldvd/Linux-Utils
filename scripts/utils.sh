#!/usr/bin/env bash

RS="\033[0m"

COLOR_WHITE="\033[0m"
COLOR_BLACK="\033[1;30m"

COLOR_RED="\033[0;31m"
COLOR_DAN="\033[0;31m"

COLOR_GREEN="\033[0;32m"
COLOR_SUC="\033[0;32m"

COLOR_YELLOW="\033[0;33m"
COLOR_WAR="\033[0;33m"

COLOR_BLUE="\033[0;34m"
COLOR_PRI="\033[0;34m"

COLOR_PURPLE="\033[0;35m"
COLOR_SEC="\033[0;35m"

COLOR_CYAN="\033[0;36m"
COLOR_INF="\033[0;36m"

COLOR_GRAY="\033[0;37m"

DOCROOT_PATH="${PROJECT_ROOT}/${DOCROOT}"

fancy_title() {
    local text1="$1"
    local text2="$2"

    text1=$(echo -n "$text1" | xargs)
    text2=$(echo -n "$text2" | xargs)

    local combined_text="${COLOR_INF}${text1}${COLOR_INF}"
    local text_length=${#text1}

    if [ -n "$text2" ]; then
        combined_text+=" ${COLOR_WAR}${text2}${COLOR_INF}"
        text_length=$((text_length + ${#text2} + 1))
    fi

    local boxWidth=$((text_length + 8 > 35 ? text_length + 8 : 35))
    local padding=$((boxWidth - text_length - 8))

    echo -e "\n${COLOR_INF}"
    printf " ┌%s┐\n" "$(printf '─%.0s' $(seq 1 $boxWidth))"
    printf " │ ▪  %b%s  ▪ │\n" "$combined_text" "$(printf '%*s' $padding '')"
    printf " └%s┘\n" "$(printf '─%.0s' $(seq 1 $boxWidth))"
    echo -e "${RS}"
}

ask_yes_no() {
    local question="$1"
    local var_name="$2"

    local textLength=$(( ${#question} + 6 ))
    local boxWidth=$(( textLength > 30 ? textLength : 30 ))

    while true; do
        echo -e "${COLOR_WAR}"
        printf "   ▪  ${COLOR_GRAY}%s ${COLOR_WAR}\n" "$question"
        printf "   └%s\n" "$(printf '─%.0s' $(seq 1 $boxWidth))"
        echo -en "${RS}   (y/n): "

        read -r response
        case "$response" in
            [Yy]*) eval "$var_name=true"; break ;;
            [Nn]*) eval "$var_name=false"; break ;;
            *) echo -e "\n${COLOR_WAR}   Invalid input. Please enter y or n.${RS}\n" ;;
        esac
    done
}

ask_text() {
    local question="$1"
    local var_name="$2"

    local textLength=$(( ${#question} + 6 ))
    local boxWidth=$(( textLength > 30 ? textLength : 30 ))

    echo -e "${COLOR_WAR}"
    printf "   ▪  ${COLOR_GRAY}%s ${COLOR_WAR}\n" "$question"
    printf "   └%s\n" "$(printf '─%.0s' $(seq 1 $boxWidth))"
    echo -en "${RS}   : "

    read -r response
    eval "$var_name='$response'"
}

rabbit() {
    local welcome_text="\n   (\\(\\ \n   (-.-)\n o_(\")(\")"
    if command -v lolcat &> /dev/null; then
        echo -e "$welcome_text" | lolcat --spread=2.0 --freq=0.3
    else
        echo -e "$welcome_text"
    fi
}

print_green() {
    echo -e "${COLOR_GREEN}!!! ${RS}$1 ${COLOR_GREEN} !!! ${RS}"
}

print_red() {
    echo -e "${COLOR_RED}!!! ${RS}$1 ${COLOR_RED} !!! ${RS}"
}

install_package() {
    local pkg="$1"
    if [ -x "$(command -v $pkg)" ]; then
        print_red "$pkg already installed"
    else
        local install_command="apt-get -y -qq install $pkg >/dev/null"
        if command -v sudo &> /dev/null; then
            install_command="sudo $install_command"
        fi
        if $install_command; then
            print_green "Successfully installed $pkg"
        else
            print_red "Failed to install $pkg"
        fi
    fi
}

function_check_install() {
    for pkg in "$@"; do
        if type -P "$pkg" &>/dev/null; then
            echo -e "- Installed - ${COLOR_GREEN}Ok${RS} - $pkg"
        else
            echo -e "- Installed - ${COLOR_RED}No${RS} - $pkg \n"
        fi
    done
}
