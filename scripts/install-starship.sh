#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

[ "$(id -u)" -eq 0 ] || { command -v sudo >/dev/null 2>&1 && SUDO=sudo; }

# |
# | ::::::: apt update
# |
fancy_title 'Run:' 'apt update'
$SUDO apt update > /dev/null

# |
# | ::::::: Installing unzip
# |
fancy_title 'Check:' 'unzip'
if ! command -v unzip &> /dev/null; then
    fancy_title 'Installing:' 'unzip'
    $SUDO apt install -y unzip
fi

# |
# | ::::::: Installing fontconfig
# |
fancy_title 'Check:' 'fontconfig'
if ! command -v fc-cache &> /dev/null; then
    fancy_title 'Installing:' 'fontconfig'
    $SUDO apt install -y fontconfig
fi

# |
# | ::::::: Installing Nerd Font
# |
fancy_title 'Install:' 'Nerd Font'
declare -a fonts=(
    "Hack"
)
VERSION='3.3.0'
FONT_DIR="${HOME}/.local/share/fonts"
mkdir -p "$FONT_DIR"
for font in "${fonts[@]}"; do
    download_url="https://github.com/ryanoasis/nerd-fonts/releases/download/v${VERSION}/${font}.zip"
    echo -e "\e[33mDownloading $download_url\e[0m"
    curl -L -o "$FONT_DIR/${font}.zip" "$download_url" && \
        unzip -o "$FONT_DIR/${font}.zip" -d "$FONT_DIR" -x "*.txt" -x "*.md" && \
        rm "$FONT_DIR/${font}.zip"
done
find "$FONT_DIR" -name '*Windows Compatible*' -delete

fc-cache -fv

# |
# | ::::::: Installing starship
# |
fancy_title 'Install:' 'starship'
curl -sS https://starship.rs/install.sh | $SUDO sh -s -- --yes

echo 'eval "$(starship init bash)"' >> "${HOME}/.bashrc"

fancy_title 'Config:' 'starship'
starship_config="$HOME/.config/starship.toml"

mkdir -p "$(dirname "$starship_config")" && > "$starship_config"
curl -sS https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/files/starship/starship-mini.toml > "$HOME/.config/starship.toml"

echo "Installation complete. Please restart your terminal or run 'source ~/.bashrc' to apply the changes."
