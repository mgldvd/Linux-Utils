#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit


SCRIPT_PATH=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

[ "$(id -u)" -eq 0 ] || SUDO=sudo

fancy_title 'update'
$SUDO apt update

export DEBIAN_FRONTEND=noninteractive
$SUDO ln -fs /usr/share/zoneinfo/Etc/UTC /etc/localtime
$SUDO dpkg-reconfigure --frontend noninteractive tzdata

packages=(
  'filezilla'
  'filezilla-common'
  'gpick'
  'gufw'
  'meld'
  'samba'
)

num_packages=${#packages[@]}

for ((i=0; i<num_packages; i++)); do
  pkg="${packages[$i]}"
  print_title "Package $((i+1))/$num_packages: $pkg"
  install_package "$pkg"
done

print_title 'Check install'

function_check_install samba
function_check_install filezilla
function_check_install meld
function_check_install gufw
function_check_install gpick
