# Installs

## First script `install-mini.sh`

- https://go.lanet.co/bashrc
- https://go.lanet.co/starship
- https://go.lanet.co/zsh

```
bash -c "$(curl -fsSL https://go.lanet.co/mini)"
```

## Install `install-init.sh`

- https://go.lanet.co/mini
- https://go.lanet.co/tz-ny
- https://go.lanet.co/fix-locale
- https://go.lanet.co/modern
- https://go.lanet.co/basics