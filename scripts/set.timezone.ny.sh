#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

fancy_title 'Set timezone' 'New York'

if command -v sudo &> /dev/null; then
    sudo ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
    sudo dpkg-reconfigure --frontend noninteractive tzdata
else
    ln -fs /usr/share/zoneinfo/America/New_York /etc/localtime
    dpkg-reconfigure --frontend noninteractive tzdata
fi

