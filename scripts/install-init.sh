#!/usr/bin/env bash

source <(curl -fsSL https://gitlab.com/mgldvd/Linux-Utils/-/raw/master/scripts/utils.sh)

rabbit

bash -c "$(curl -fsSL https://go.lanet.co/mini)"

bash -c "$(curl -fsSL https://go.lanet.co/tz-ny)"

bash -c "$(curl -fsSL https://go.lanet.co/fix-locale)"


fancy_title 'Install' 'modern cli tools'
bash -c "$(curl -fsSL https://go.lanet.co/modern)"

fancy_title 'Install' 'Basics'
bash -c "$(curl -fsSL https://go.lanet.co/basics)"

